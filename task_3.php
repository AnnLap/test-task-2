<?php
//Задание №3. Рассматривала только правильные пирамиды.

abstract class FigureClass //абстрактная фигура 
{      
    public $square, $x, $y, $z; //площадь, параметр 1, параметр 2, параметр 3
 
    public function metodAssignment ($x, $y, $z) {
        $this->x = $x;
        $this->y = $y;
		$this->z = $z;
    }
 
    protected abstract function metodSquare();
		
  
    public function metodReturn () {
	   return $this->square;
	   
    }
}

class RectangleClass extends FigureClass 
{
	function metodSquare () { // площадь прямоугольника
        $this->square = $this->x*$this->y;
	 }
}

class CircleClass extends FigureClass 
{
	function metodSquare () { // площадь круга
        $this->square = ($this->x*$this->x)*M_PI;
	 }
}

class PyramidClass extends FigureClass 
{
	 function metodSquare () { // площадь поверхности пирамиды (только для правильной пирамиды)
        $this->square = (($this->z*$this->x)/2)*($this->x/(2*Tan(deg2rad(180/$this->z))) + Sqrt(Pow($this->y,2)+Pow(($this->x/(2*Tan(deg2rad(180/$this->z)))),2)));
	 }
}
 
function make_seed()
{
    list($usec, $sec) = explode(' ', microtime());
    return (float) $sec + ((float) $usec * 100000);
}


//------------------------------------------------------------------------------------------------
//Создание случайных объектов:
echo "Случайные объекты.<br>";

$a = 0; $b = 0; $c = 0; $d = 0; $sq = 0; //параметры, описывающие фигуру, первые три - те, которые передаются на вход в metodAssignment, $d - тип фигуры (прямоугольник, круг, пирамида), $sq - площадь фигуры
$arrayObject = array(); //массив объектов, состоящий из массивов параметров для каждого объекта
$n = 5; //колличество создаваемых объектов

for ($i = 0; $i < $n; $i++) {
	srand(make_seed());
	$d = rand(1,3); //рандомные числа 1,2,3 - от этого зависит какая будет фигура
	
	//Прямоугольник:
	if ($d == 1) {
		$square = new RectangleClass; 
		srand(make_seed());
	    $a = rand(1,10); //рандомные числа от 1 до 10 ради удобства, но можно другие
		srand(make_seed());
	    $b = rand(1,10);
        $c = 0;
		$square->metodAssignment($a,$b,0); //параметры: ширина, длина, null
		$square->metodSquare(); 
		$rectangleSquare = $square->metodReturn();
		unset($square); 
		echo "Прямоугольник: ", "ширина = ", $a, ", высота = ", $b, ", площадь = ", $rectangleSquare,"<br>"; //сразу вывожу что и с каким параметром сгенерировалось
		$arrayObject[$i] = array('X'=>$a, 'Y'=>$b, 'Z'=>$c, 'Figure'=>'Прямоугольник', 'Square'=>$rectangleSquare);
		
	}
	
	//Круг:
	elseif ($d == 2) {
		$square = new CircleClass; 
		srand(make_seed());
	    $a = rand(1,10);
        $b = 0;
        $c = 0;
		$square->metodAssignment($a,0,0); //параметры: радиус окружности, null, null
		$square->metodSquare(); 
		$сircleSquare = $square->metodReturn();
		unset($square);
		echo "Круг: ", "радиус = ", $a,", площадь = ", $сircleSquare, "<br>";
		$arrayObject[$i] = array('X'=>$a, 'Y'=>$b, 'Z'=>$c, 'Figure'=>'Круг', 'Square'=>$сircleSquare);
	}
	
	//Пирамида:
	elseif ($d == 3) {
	$square = new PyramidClass;
	srand(make_seed());
	$a = rand(1,10);
	srand(make_seed());
	$b = rand(1,10);
	srand(make_seed());
	$c = rand(1,10);
	$square->metodAssignment($a,$b,$c); //параметры: сторона основания, высота пирамиды, число сторон основания 
	$square->metodSquare(); 
	$pyramidSquare = $square->metodReturn();
	unset($square);
	echo "Пирамида: ", "сторона основания = ", $a, ", высота = ", $b, ", число сторон основания = ", $c,", площадь = ", $pyramidSquare, "<br>"; 
	$arrayObject[$i] = array('X'=>$a, 'Y'=>$b, 'Z'=>$c, 'Figure'=>'Пирамида', 'Square'=>$pyramidSquare);
	}
	
}
//print_r ($arrayObject); //можно вывести массив описания объекта, который записывается в файл



//------------------------------------------------------------------------------------------------
//Сортировка по размеру площади:
echo "Сортировка.<br>";

$arrayObjectSort = $arrayObject;
for($i=0; $i<$n; $i++){
    for($j=$i+1; $j<$n; $j++) {
        $buf1 = $arrayObjectSort[$i]["Square"];
        $buf2 = $arrayObjectSort[$j]["Square"];
        if($buf1 > $buf2){
                   $temp = $arrayObjectSort[$j];
                   $arrayObjectSort[$j] = $arrayObjectSort[$i];
                   $arrayObjectSort[$i] = $temp; 
        }
    }
}

for($i=0; $i<$n; $i++){ //вывод отсортированных фигур
echo $arrayObjectSort[$i]["Figure"], " S= ",$arrayObjectSort[$i]["Square"], "<br>"; 
}



//------------------------------------------------------------------------------------------------
//Запись в файл и считывание из него
$filename = 'fileArray.txt';
if (!$file_handle = fopen($filename, 'wb')) exit;
flock($file_handle, LOCK_EX);
if (fwrite($file_handle, serialize($arrayObjectSort)) === false) exit;
flock($file_handle, LOCK_UN);
fclose($file_handle);

echo "Фигуры успешно сохранены! <br>";

if ( !$file_handle = fopen($filename, 'rb') ) exit;
$arrayObjectSortNew = unserialize( fread($file_handle, filesize($filename)) );
fclose($file_handle);

echo "Фигуры из файла: <br>";
for($i=0; $i<$n; $i++){ //вывод отсортированных фигур
echo $arrayObjectSortNew[$i]["Figure"], " S= ",$arrayObjectSort[$i]["Square"], "<br>"; 
}

?>